import React, {useCallback, useContext, useState} from 'react';
import Base from '../../Components/Base';
import Card from '../../Components/Card';
import {Text, TouchableOpacity, View} from "react-native";
import {loginApi} from "../../API/Methods/Authentication";
import Styles from "./Login-Style";
import {LoginContext} from "../../Context/LoginContext";


function Login (props) {
    const {dispatch} = useContext(LoginContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);

    //Login Function
    const login = useCallback(async () => {
        if (email === "") {
            alert("The email field is required");
            return;
        }
        if (password === "") {
            alert("The password field is required");
            return;
        }
        setLoading(true);
        const response = await loginApi(email, password);
        if (response.status === 422) {
            let text = ''
            for (const [key, value] of Object.entries(response)) {
                text = text + value + '\n'
            }
            alert(text);
        }
        if (response.status === 200) {
            dispatch({ type: 'SIGN_IN', Token: response.token })
        }
        setLoading(false);
    } ,[email , password])

     const setPass = useCallback((password) => setPassword(password), []);

     const setEmailAddress = useCallback((email) => setEmail(email), []);

     //For Registeration
     const goToSignUp = useCallback(() => props.navigation.navigate('Signup'), []);

    return (
        <Base style={Styles.base}>
            <Card
                onPress={login}
                title={'Login'}
                buttonTitle={'Sign In'}
                loading={loading}
                setPassword={setPass}
                setEmail={setEmailAddress}
            >
                <TouchableOpacity style={Styles.signupLink} onPress={goToSignUp} >
                    <Text style={Styles.signupText}>
                        {'Sign Up'}
                    </Text>
                </TouchableOpacity>
            </Card>
        </Base>
    );
}

export default Login;
