import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import LoginContextProvider from "./Src/Context/LoginContext";
import BookMarkContextProvider from "./Src/Context/BookmarkContext";
import MainStack from "./Src/Navigation/MainStack";


function App() {
    return (
        <SafeAreaProvider>
            <LoginContextProvider>
                <BookMarkContextProvider>
                    <NavigationContainer>
                        <MainStack/>
                    </NavigationContainer>
                </BookMarkContextProvider>
            </LoginContextProvider>
        </SafeAreaProvider>
    )
}

export default App;






