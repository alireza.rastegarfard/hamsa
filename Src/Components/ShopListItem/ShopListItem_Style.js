import {StyleSheet} from 'react-native';

const Styles = StyleSheet.create({
    card:{
        width: '93%',
        alignSelf: 'center',
        borderRadius: 10,
        padding: 10,
        backgroundColor: 'white',
        marginTop: 15
    },
    productImage:{width: '100%', height: 150, borderRadius: 10},
    descriptionView:{flexDirection: 'row'},
    productDetailsView:{width: '90%', paddingLeft: 10},
    bookmarkButton:{width: '10%', alignItems: 'center', justifyContent: 'center'},
    bookmarkIcon:{width: 25, height: 30},
    title:{fontSize: 20, marginTop: 10}

});
export default Styles;
