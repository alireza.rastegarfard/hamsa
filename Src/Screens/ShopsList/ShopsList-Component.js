import React, {useCallback, useContext, useEffect, useState} from 'react';
import Base from '../../Components/Base';
import ShopListItem from '../../Components/ShopListItem';
import {getShopListApi} from "../../API/Methods/Shop";
import {ActivityIndicator, FlatList, RefreshControl} from "react-native";
import Constant from "../../PublicData/ConstValue";
import {LoginContext} from "../../Context/LoginContext";
import CustomButton from "../../Components/CustomButton/CustomButton-Component";
import Colors from "../../PublicData/Colors";
import Styles from "./ShopList-Style";

function ShopsList(props) {
    const [loading, setLoading] = useState(false);
    const [page, setPage] = useState(1);
    const [refreshing, setRefreshing] = useState(false);
    const [shopData, setShopData] = useState([]);
    const [endReached, set_endReached] = useState(false)
    const {Token,dispatch} = useContext(LoginContext);
//Get Shop List
    const getShop = useCallback(async () => {
        setLoading(true);
        const response = await getShopListApi( page , Token?.Token);
        setLoading(false);
        if (response.status === 422) {
            let text = ''
            for (const [key, value] of Object.entries(response)) {
                text = text + value + '\n'
            }
            alert(text);
        }
        if (response.status === 200) {
            setShopData([...shopData, ...response?.data]);
        }
        if (response.status === 401) {
            dispatch({type: 'SIGN_OUT'});
        }
    },[Token?.Token , endReached ,loading , shopData , page])

    const renderItem = useCallback(({item}) => {
        return (<ShopListItem data={item}/>)
    } ,[])

    const keyExtractor = useCallback(item => item.id.toString(),[])
// handle lazy load
    const onEndPage = useCallback(() => {
        if (shopData.length >= Constant.PAGE_COUNT_BIGGER) {
            setPage(page + 1)
            getShop()
        }
        set_endReached(true)
    },[shopData ,page])


    const listFooterComponent = useCallback(() =>
        !endReached &&
        <ActivityIndicator
            style={Styles.activityIndicatorStyle}
             size="small" color={Colors.blue}/> , [endReached])

    const shopsOnRefresh = useCallback(() => {
        setRefreshing(true);
        getShop()
        setTimeout(() => setRefreshing(false), Constant.TIMEOUT * 1000);
    }, []);

    const goToDashboard = () => {
        props.navigation.navigate('Dashboard')
    };

    useEffect(() => {
        getShop().done();
    }, [])
    return (
        <Base>
            <CustomButton
                onPress={goToDashboard}
                title={'dashboard'}/>
            <FlatList
                data={shopData}
                renderItem={renderItem}
                keyExtractor={keyExtractor}
                onEndReached={onEndPage}
                ListFooterComponent={listFooterComponent}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={shopsOnRefresh}
                    />
                }
            />
        </Base>

    );
}

export default ShopsList;
