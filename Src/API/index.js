let BASE_URL='https://challenge.hamsaa.ir/api';
export default async function api(
    path,
    params,
    token = null,
    method = 'post',

) {
    let options;
    options = {
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            ...(token && { Authorization: `Bearer ${token}` }),
        },
        method,
        ...(params && { body: JSON.stringify(params) }),
    };
    try {
        const response = await fetch(BASE_URL + path, options);
        const data = await response.json();
        return {
            ...data,
            status: response.status,
        };
    } catch (error) {

        return error;
    }
}
