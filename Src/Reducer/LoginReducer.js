
export const LoginReducer = (prevState, action) => {
    switch (action.type) {
        case 'RESTORE_TOKEN':
            return {
                ...prevState,
                Token: action.Token,
            };
        case 'SIGN_IN':
            return {
                ...prevState,
                Token: action.Token,
            };
        case 'SIGN_OUT':
            return {
                ...prevState,
                Token: null,
            };
    }
};
