import {StyleSheet} from 'react-native';
import {PublicStyles} from "../../PublicData/Style/PublicStyle";

const Styles = StyleSheet.create({
    base:{...PublicStyles.base},

});
export default Styles;
