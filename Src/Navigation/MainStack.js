import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Login from '../Screens/Login';
import Signup from '../Screens/Signup';
import ShopsList from "../Screens/ShopsList";
import {LoginContext} from "../Context/LoginContext";
import Dashboard from "../Screens/Dashboard";

const Stack = createNativeStackNavigator();

function MainStack() {
    function getStack(value) {
        return value.Token.Token == null ? (
            <Stack.Navigator>
                <Stack.Screen name={'Login'} component={Login}/>
                <Stack.Screen name={'Signup'} component={Signup}/>
            </Stack.Navigator>
        ) : (
            <Stack.Navigator>
                <Stack.Screen name={'ShopsList'} component={ShopsList}/>
                <Stack.Screen name={'Dashboard'} component={Dashboard}/>
            </Stack.Navigator>
        )
    }

    return (
        <LoginContext.Consumer>
            {value => getStack(value)}
        </LoginContext.Consumer>

    )

}

export default MainStack;






