import {StyleSheet} from 'react-native';

const Styles = StyleSheet.create({
    cardView:{width: '80%', overflow: 'hidden', borderRadius: 10, backgroundColor: '#FFFFFF'},
    titleView:{
        height: 60,
        paddingLeft: 20,
        justifyContent: 'center',
        borderBottomColor: '#898989',
        borderBottomWidth: 1
    },
    title:{fontSize: 24},
    inputsView:{paddingBottom: 20}

});
export default Styles;
