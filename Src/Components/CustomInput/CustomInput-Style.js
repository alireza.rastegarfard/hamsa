import {StyleSheet} from 'react-native';
import Colors from "../../PublicData/Colors";

const Styles = StyleSheet.create({
    inputView:{
        borderColor: Colors.blue,
        borderWidth: 1,
        height: 45,
        width: '80%',
        marginTop: 20,
        alignSelf: 'center',
        borderRadius:5
    },
    input:{
        paddingLeft: 10,
        width: '100%',
        height:'100%',
        color: Colors.black
    }

});
export default Styles;
