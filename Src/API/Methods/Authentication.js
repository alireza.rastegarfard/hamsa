import api from "../index";
let register='/account/register';
let login='/account/login';

export const registerApi = (email,password) =>
    api(register, { email,password  });

export const loginApi = (email,password) =>
    api(login, { email,password  });
