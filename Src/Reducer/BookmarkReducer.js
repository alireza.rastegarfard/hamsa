

export const BookmarkReducer = (state, action) => {
    switch (action.type) {
        case 'ADD_BOOKMARK_ARRAY_LIST':
            return [...state,...action.data];
        case 'ADD_BOOKMARK_LIST':
            return [
                ...state,
                {
                    name: action.data.name,
                    image: action.data.image,
                    id: action.data.id,
                }
            ];
        case 'REMOVE_BOOKMARK':
            return state.filter(bookMark => bookMark.id !== action.id);
        default:
            return state;
    }
};
