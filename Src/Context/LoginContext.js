import React, {createContext, useReducer, useEffect} from 'react';
import {LoginReducer} from "../Reducer/LoginReducer";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {UseIsMount} from "../PublicData/Function/UseIsMount";

export const LoginContext = createContext();

const LoginContextProvider = props => {
    const isMount = UseIsMount();
    const [Token, dispatch] = React.useReducer(
        LoginReducer,
        {
            Token: null,
        }
    );

    useEffect(() => {
        if (isMount) {
            return;
        }
        if(Token?.Token===null){
            AsyncStorage.setItem('UserToken', "")
        }else {
            AsyncStorage.setItem('UserToken', Token?.Token);
        }
    }, [Token]);

    useEffect(() => {
        const init = async () => {
            let UserToken = await AsyncStorage.getItem('UserToken');
            if(UserToken!==null)
            dispatch({type: 'SIGN_IN',Token:UserToken});
        };

        init();
    }, []);

    return (
        <LoginContext.Provider value={{Token, dispatch}}>
            {props.children}
        </LoginContext.Provider>
    );
};

export default LoginContextProvider;
