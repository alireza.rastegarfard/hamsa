import {StyleSheet} from 'react-native';
import Colors from "../../PublicData/Colors";

const Styles = StyleSheet.create({
    button: {
        backgroundColor:Colors.blue,
        height: 45,
        width: '80%',
        marginTop: 20,
        alignSelf: 'center',
        borderRadius: 5,
        alignItems: 'center', justifyContent: 'center'
    },
    title: {color: Colors.white}
});
export default Styles;
