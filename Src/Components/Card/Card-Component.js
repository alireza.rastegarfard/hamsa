import React, {useCallback} from 'react';
import CustomInput from '../../Components/CustomInput';
import CustomButton from '../../Components/CustomButton';
import {Text, View} from "react-native";
import PropTypes from "prop-types";
import Styles from "./Card-Style";


const Card = (props) => {

    const setPass = useCallback((password) => props.setPassword(password), []);

    const setEmailAddress = useCallback((email) =>  props.setEmail(email), []);

    return (
        <View style={Styles.cardView}>
            <View style={Styles.titleView}>
                <Text style={Styles.title}>
                    {props.title}
                </Text>
            </View>
            <View style={Styles.inputsView}>
                <CustomInput
                    placeholder={'email'}
                    returnKeyType={'done'}
                    onChangeText={setEmailAddress}
                />
                <CustomInput
                    secureTextEntry
                    placeholder={'password'}
                    returnKeyType={'done'}
                    onChangeText={setPass}
                />
                <CustomButton
                    loading={props.loading}
                    {...props}
                    title={props.buttonTitle}/>
                {props.children}
            </View>
        </View>

    );
}

export default React.memo(Card);

Card.propTypes = {
    title: PropTypes.string,
    buttonTitle: PropTypes.string,
    loading:PropTypes.bool,
    setPassword:PropTypes.func,
    setEmail:PropTypes.func
};

Card.defaultProps = {};
