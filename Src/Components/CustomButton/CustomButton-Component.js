import React, {useCallback} from 'react';
import {ActivityIndicator, Text, TouchableOpacity} from "react-native";
import PropTypes from "prop-types";
import Styles from "./CustomButton-Style";
import Colors from "../../PublicData/Colors";

//global Button Format
const CustomButton = (props) => {
    //prevent extra re rendering when call function in jsx
    const handleLoading = useCallback(() => {
        if (props.loading) {
            return <ActivityIndicator color={Colors.white}/>;
        } else {
            return <Text style={Styles.title}>
                {props.title}
            </Text>;
        }
    }, [props.loading, props.title]);

    return (
        <TouchableOpacity style={Styles.button} {...props} >
            { handleLoading() }
        </TouchableOpacity>
    )
};

export default React.memo(CustomButton);

CustomButton.propTypes = {
    title: PropTypes.string,
    loading: PropTypes.bool,
};

CustomButton.defaultProps = {};
