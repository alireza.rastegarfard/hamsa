import {StyleSheet} from 'react-native';
import {PublicStyles} from "../../PublicData/Style/PublicStyle";

const Styles = StyleSheet.create({
    base:{...PublicStyles.base},
    signupLink:{alignSelf: 'center', marginTop: 15},
    signupText:{textDecorationLine: 'underline'}
});
export default Styles;
