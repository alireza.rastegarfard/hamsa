import React, {createContext, useReducer, useEffect} from 'react';
import {BookmarkReducer} from '../Reducer/BookmarkReducer';
import AsyncStorage from "@react-native-async-storage/async-storage";
import {UseIsMount} from "../PublicData/Function/UseIsMount";

export const BookMarkContext = createContext();

//BookMark Context
const BookMarkContextProvider = props => {
    const isMount = UseIsMount();
    const [bookMarks, dispatch] = useReducer(BookmarkReducer, []);

    useEffect(() => {
        if (isMount) {
            return;
        }
        AsyncStorage.setItem('bookmark', JSON.stringify(bookMarks));
    }, [bookMarks]);

    useEffect(() => {
        const init = async () => {
            let bookmark = await AsyncStorage.getItem('bookmark');
            bookmark = JSON.parse(bookmark);
            if (bookmark != null) {
                if (bookmark.length > 0)
                    dispatch({type: 'ADD_BOOKMARK_ARRAY_LIST', data: bookmark});
            }
        };
        init();
    }, []);

    return (
        <BookMarkContext.Provider value={{bookMarks, dispatch}}>
            {props.children}
        </BookMarkContext.Provider>
    );
};

export default BookMarkContextProvider;
