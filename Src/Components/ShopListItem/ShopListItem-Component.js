import React, {useCallback, useContext} from 'react';
import {Image, Text, TouchableOpacity, View} from "react-native";
import Images from "../../Assets/Images";
import Styles from "./ShopListItem_Style";
import {BookMarkContext} from "../../Context/BookmarkContext";
import PropTypes from "prop-types";

//PerItem Of ShopList
const ShopListItem = (props) => {
    const {bookMarks, dispatch} = useContext(BookMarkContext);

    //find book mark products
    const isBookMarked = bookMarks.findIndex(items => items.id === props.data?.id);

    //add to book mark and inverse
    const bookMarkManage = useCallback(() => {
            if (isBookMarked !== -1) {
                dispatch({type: 'REMOVE_BOOKMARK', id: props.data.id})
            } else {
                dispatch({type: 'ADD_BOOKMARK_LIST', data: props.data})
            }
    }, [isBookMarked]);

    return (
        <View style={Styles.card}>
            <Image
                resizeMode={'cover'}
                style={Styles.productImage}
                source={props.data?.image?{uri: props.data.image}:Images.noImage} />
            <View style={Styles.descriptionView}>
                <View style={Styles.productDetailsView}>
                    <Text style={Styles.title}>
                        {props.data.name}
                    </Text>
                </View>
                <TouchableOpacity
                    onPress={bookMarkManage}
                    style={Styles.bookmarkButton}>
                    <Image
                        resizeMode={'contain'}
                        style={Styles.bookmarkIcon}
                        source={isBookMarked !== -1 ? Images.bookmarkDark : Images.bookmark}/>
                </TouchableOpacity>
            </View>
        </View>
    )
};

export default React.memo(ShopListItem);

ShopListItem.propTypes = {
    items: PropTypes.object
};

ShopListItem.defaultProps = {};
