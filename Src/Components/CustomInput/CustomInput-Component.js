import React from 'react';
import {TextInput, View} from "react-native";
import Styles from "./CustomInput-Style";
import Colors from "../../PublicData/Colors";

//Global Input Format
const CustomInput = (props) => {
    return (
        <View style={Styles.inputView}>
            <TextInput
                {...props}
                style={Styles.input}
                placeholderTextColor={Colors.blue}
            />
        </View>
    )
};

export default React.memo(CustomInput);

CustomInput.propTypes = {};

CustomInput.defaultProps = {};
