

const Images = {
    bookmark: require('./bookmark.png'),
    bookmarkDark: require('./bookmarkDark.png'),
    noImage: require('./noImage.png'),
};

export default Images;
