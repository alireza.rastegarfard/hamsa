const Colors = {
    blue: '#1E208D',
    black: '#313235',
    white: '#fff',
    lightGray: '#EDEEF3FF',
};

export default Colors;
