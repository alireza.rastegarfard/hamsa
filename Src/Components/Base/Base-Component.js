import React, {useMemo} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import PropTypes from "prop-types";
import Styles from "./Base-Style";

//Main Background App
const Base = (props) => {
    //prevent extra re rendering when use merge style data
    const BaseViewStyle = useMemo(
        () => ({
            ...Styles.main,
            ...props.style,
        }),
        [props.style],
    );
    return <SafeAreaView
        style={BaseViewStyle}>
        {props.children}
    </SafeAreaView>;
};

export default React.memo(Base);

Base.propTypes = {
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
};

Base.defaultProps = {
    style: {},
};
