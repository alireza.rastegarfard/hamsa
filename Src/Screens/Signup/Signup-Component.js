import React, {useCallback, useState} from 'react';
import Base from '../../Components/Base';
import Card from '../../Components/Card';
import {Alert} from "react-native";
import {registerApi} from "../../API/Methods/Authentication";
import Styles from "./Signup-Style";

function Signup(props) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);

    // register user
    const register = useCallback(async () => {
        if (email === "") {
            alert("The email field is required");
            return;
        }
        if (password === "") {
            alert("The password field is required");
            return;
        }
        setLoading(true);
        const response = await registerApi(email, password);
        setLoading(false);
        if (response.status === 422) {
            let text = ''
            for (const [key, value] of Object.entries(response)) {
                text = text + value + '\n'
            }
            alert(text);
        }
        if (response.status === 201) {
            Alert.alert(
                "registration completed successfully",
                "back to Login Page",
                [
                    {text: "OK", onPress: () => props.navigation.goBack()}
                ]
            );
        }
        setLoading(false);
    }, [email,password]);
    const setEmailAddress = useCallback( (email) => setEmail(email),[])
    const setPass = useCallback( (password) => setPassword(password),[])

    return (
        <Base style={Styles.base}>
            <Card
                setPassword={setPass}
                setEmail={setEmailAddress}
                loading={loading}
                title={'register'}
                buttonTitle={'Sign Up'}
                onPress={register}
            >
            </Card>
        </Base>
    );
}

export default Signup;
