<h1 align="center">
  <a >
  Shop
  </a>
</h1>

<p align="center">
  <strong>This is a test project for hamsa</strong><br>
  developed in react native
</p>





This is a task that was assigned to me (Alireza Rastegarfard).


The task is to create a minimal app which consists of register, login, shops list and
dashboard pages. You need to create two simple page for user authentication (Sign up, Log in). In the app's home page, you will show the list of shops (from the API), and allow the user to see and interact with "Add To Favorites" button if the user is logged in. Once user adds some of the shops to his favorites, he then should be able to remove those selected shops from the favorites as well.

Finally, the user can view his favorite shops in his dashboard. Also he must be able to log out from his dashboard.


## Contents

- [Requirements](#-requirements)
- [ Running the project](#-Running-the-project)
- [Directory layout](#-Directory-layout)
- [Components and libraries](#-Components-and-libraries)
- [Documentation](#-Documentation)
- [How to Contribute](#-how-to-contribute)



## 📋 Requirements

React Native apps may target iOS 11.0 and Android 5.0 (API 21) or newer. You may use Windows, macOS, or Linux as your development operating system, though building and running iOS apps is limited to macOS.

## Running the project

Assuming you have all the requirements installed, you can setup and run the project by running:

- `npm install` to install the dependencies
- run the following steps for your platform

### Android

- you need to use:
    - `react-native run-android`


### iOS

- `cd ios`
- `pod install` to install pod dependencies
- `cd ..` to come back to the root folder
- `yarn start` to start the metro bundler, in a dedicated terminal
- `yarn ios` to run the iOS application (remember to start a simulator or connect an iPhone phone)


## Directory layout

- [`Src/API`](Src/API): presentational components
- [`Src/Assets`](Src/Assets):  assets (image, other files, ...) used by the application
- [`Src/Components`](Src/Components):  presentational components
- [`Src/Context`](Src/Context): react context
- [`Src/Navigation`](Src/Navigation): react navigation navigators
- [`Src/PublicData`](Src/PublicData):  configuration of the application
- [`Src/Reducer`](Src/Reducer): App states
- [`Src/Screens`](Src/Screens):  container components, i.e. the application's screens


## Components and libraries

The boilerplate contains:

- [React Native](https://facebook.github.io/react-native/) (v**0.66.4**) 
- [async-storage](https://www.npmjs.com/package/@react-native-async-storage/async-storage) (v1.15.14) An asynchronous, unencrypted, persistent, key-value storage system for React Native.
- [react-native-safe-area-context](https://www.npmjs.com/package/react-native-safe-area-context) (v3.3.2) A flexible way to handle safe area, also works on Android and Web!
- [React Navigation](https://reactnavigation.org/) (v6.0.6)  to handle routing and navigation in the app

## 📖 Documentation

The full documentation for this project can be found on  [website](https://curvy-kip-950.notion.site/Front-End-developer-test-6a84b21e9ab14ae3808ef0780250b303).



## 👏 How to Contribute

This is a private project and there is no plan for contributing







