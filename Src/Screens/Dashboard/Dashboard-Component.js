import React, {useCallback, useContext} from 'react';
import Base from '../../Components/Base';
import {FlatList} from "react-native";
import CustomButton from "../../Components/CustomButton/CustomButton-Component";
import ShopListItem from "../../Components/ShopListItem/ShopListItem-Component";
import {BookMarkContext} from "../../Context/BookmarkContext";
import {LoginContext} from "../../Context/LoginContext";


function Dashboard () {
    const {bookMarks} = useContext(BookMarkContext);
    const {dispatch} = useContext(LoginContext);

    const keyExtractor = useCallback(item => item.id.toString(),[])

    //PerItem BookMarkItems
    const renderItem = useCallback(({item}) => {
        return (<ShopListItem data={item}/>)
    },[])

    //singUp Function
    const signUp = useCallback(() => {
        dispatch({type: 'SIGN_OUT'})
    },[])

    return (
        <Base>
            <CustomButton
                onPress={signUp}
                title={'sign out'}/>
            <FlatList
                data={bookMarks}
                renderItem={renderItem}
                keyExtractor={keyExtractor}
            />
        </Base>
    );
}

export default Dashboard;
